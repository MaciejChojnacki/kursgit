// Definicja większości elementów:
const pageContent = document.querySelector("#page-content");
const lightButton = document.querySelector("#button-lightMode");
const darkButton = document.querySelector("#button-darkMode");
const scrollList = document.querySelector("#scroll-list");
const scrollListElements = document.querySelectorAll(".scroll-list-elements");
const scroll = document.querySelectorAll(".scroll");

// Scroll strony, przy pomocy elementów, do konkretnych tematów:
for (let i = 0; i < scrollListElements.length; i++) {
    scrollListElements[i].addEventListener('click', () => {
        scroll[i].scrollIntoView({behavior: "smooth"});
    });
}

// Tryb jasny:
lightButton.addEventListener('click', () => {
    document.documentElement.style.setProperty('--theme-color1', '#EEEEEE');
    document.documentElement.style.setProperty('--theme-color2', '#E0E0E0');
    document.documentElement.style.setProperty('--theme-color-text', '#000000');
    pageContent.style.boxShadow = "0px 15px 15px -10px #000000";
    scrollList.style.boxShadow = "0px 15px 15px -10px #000000";
});

// Tryb ciemny:
darkButton.addEventListener('click', () => {
    document.documentElement.style.setProperty('--theme-color1', '#212121');
    document.documentElement.style.setProperty('--theme-color2', '#181818');
    document.documentElement.style.setProperty('--theme-color-text', '#ffffff');
    pageContent.style.boxShadow = "0 15px 15px -10px #ffffff";
    scrollList.style.boxShadow = "0 15px 15px -10px #ffffff";
});

// Powtarzalna zmiana koloru elementów:
setInterval(() => {
    let randomColor = `hsl(${Math.random() * 360}, 80%, 70%)`;
    document.documentElement.style.setProperty('--random-color', randomColor);
},  1000);

